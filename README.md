# 44-663: Instructions

Persistence with ASP.NET 5: Client to Server to Data Store and back.

Practice storing data

[Northwest Online](http://www.nwmissouri.edu/online/)




## A06 must employ:

*   ASP.NET 5
*   MVC 6
*   Entity Framework 7
*   SQL Server LocalDB

## Getting Started

*   Create new project DataYourname.
*   Use ASP.NET 5 Web Application Template with No Authentication.



## D06: Start your App

*   Add EF 7 and SQL Server.
*   Add EF commands.
*   Add Models folder.
*   Add the Location model provided.
*   Create a new, custom entity that requires a location.
*   Examples include Movie, Book, Actor, Aquarium, Restaurant, Event, etc.
*   You may use your project entity if you like - but your selection must be unique.
*   You may use add entities if needed to get to something that uses a location attribute.
*   Find or generate data to create at least 7 instances of each class.
*   Use appropriate annotations and validation.
*   Make any additions to the Location class required.
*   Create your AppDBContext.
*   Make sure it runs and post a picture of your new application running in D06\. Include the code for your location-enabled entity.
*   If you enhanced the location model, insert your code for Location.cs as well.



## C06: Seed your Data

*   Create an AppSeedData class.
*   Create an Initialize method.
*   If the context is null, throw an exception.
*   If the database is already seeded, return.
*   Otherwise, add seed data for both locations and your custom class.
*   Add Dependency Injection to your application.
*   Setup your database connection string in the appropriate appsettings file.
*   In Startup.cs, update ConfgureServices to add EF and Logging services.
*   In Startup.cs, update Configure to call your static method AppSeedData.Initialize.
*   In a command window in the proper folder, set the dn version to use.
*   Then, add your initial migration (a set of changes to initialize or modify the schema).
*   Then, update your database. Refresh and view your tables in the Visual Studio SQL Server Object Explorer (SSOE).
*   Make sure your code runs. Post a picture of it running in C06\. Include a screenshot of your tables in your VS SSOE.



## A06 - Your Final Persistent Application

*   Scaffold your location controller and views.
*   Scaffold your custom class(es) controller and views.
*   Modify your navigation.
*   Update or remove the old About and Contact pages.
*   Update the content on the Index page to be more suitable for your application.
*   Work together with your team to discuss how you want to navigate in your team project. What will be the opening view? What screens are needed? What parts does your MVC project application need?
*   Update your views to get closer to the way you want your project to work.
*   Make sure your code runs. Commit your code in our BitBucket team repo under A06 (or create private one and invite us if you prefer). Post a link to your repo and at least 3 screen shots of your Data app running.

## To run this version

To run this version, you'll want to set up your own database.

First, delete all the files in the Migrations folder and recreate them. Second, in the project folder, in a command window, check to see if you have 1.0.0-rc1-update1 installed already.  If not, install it:

```
dnvm list
dnvm install 1.0.0-rc1-update1
```

Third, set the version, create a new InitialMigration, and update the database:

```
dnvm use 1.0.0-rc1-final
dnu restore
dnx ef migrations add InitialMigration
dnx ef database update
``` 

Fourth, refresh your database in the SQL Server Object Explorer. You should see the Location table.  You can now run the app. 

[Northwest Online](http://www.nwmissouri.edu/online/)

## Example behavior

![](images/2016-02-14_2021.png) 

![](images/2016-02-14_2022.png)