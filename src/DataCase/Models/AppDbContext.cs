﻿using Microsoft.Data.Entity;


namespace DataCase.Models
{
    public class AppDbContext : DbContext {
       public DbSet<Location> Locations { get; set; }
        public DbSet<Movie> Movies { get; set; }

    }
}
